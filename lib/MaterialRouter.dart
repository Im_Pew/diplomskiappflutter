import 'package:flutter/material.dart';
import 'package:zavrsniapp/Router.dart';
import 'package:zavrsniapp/details/DetailsController.dart';
import 'package:zavrsniapp/main_list/ListController.dart';

class MaterialRouter extends Router {
  @override
  Route generateRoute(RouteSettings settings) {
    if (settings.name == Router.DETAILS_ROUTE) {
      return MaterialPageRoute(
          builder: (_) => DetailsController(pokemon: settings.arguments));
    }
    return MaterialPageRoute(builder: (_) => ListController());
  }
}
