import 'package:http/http.dart' as http;

class RemoteDataProvider {
  static const String SPRITES_URL =
      "https://courses.cs.washington.edu/courses/cse154/webservices/pokedex/sprites/\$pok.png";

  static String getSpriteUrl(String name) {
    var url = SPRITES_URL;

    return url.replaceAll("\$pok", name);
  }

  Future<http.Response> fetchAllPokemons() {
    return http.get("https://pokeapi.co/api/v2/pokemon?limit=151");
  }

  Future<http.Response> fetchPokemon(String name) {
    return http.get(
        "https://courses.cs.washington.edu/courses/cse154/webservices/pokedex/pokedex.php?pokemon=" +
            name);
  }
}
