import 'package:recase/recase.dart';

import '';

import 'package:zavrsniapp/data_provider/RemoteDataProvider.dart';

class PokemonList {
  int id;
  String name;
  String codeName;
  String url;

  PokemonList({this.id, this.name, this.codeName,  this.url});

  factory PokemonList.fromJson(Map<String, dynamic> json) {
    String url = json["url"] as String;
    List<String> spliced = (json["url"] as String).split("\/");

    String pokemonName = json["name"] as String;
    String iconUrl = RemoteDataProvider.getSpriteUrl(pokemonName);

    pokemonName = pokemonName.replaceAll("-", " ");
    pokemonName = ReCase(pokemonName).titleCase;

    print("Last item is: " + spliced[spliced.length - 2]);
    return PokemonList(
        id: int.parse(spliced[spliced.length - 2]), name: pokemonName, codeName: json["name"] as String, url: iconUrl);
  }
}
