import 'Images.dart';
import 'Info.dart';

class Pokemon {
  String name;
  int hp;
  Info info;
  Images images;

  Pokemon({this.name, this.hp, this.info, this.images});

  factory Pokemon.fromJson(Map<String, dynamic> json) {
    Info info = Info.fromJson(json["info"]);
    Images images = Images.fromJson(json["images"]);

    return Pokemon(
        name: json["name"] as String,
        hp: json["hp"] as int,
        info: info,
        images: images);
  }
}
