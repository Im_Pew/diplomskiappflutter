import 'package:zavrsniapp/data_models/PokemonList.dart';

class PokemonListResponse {
  int count;
  String next;
  String previous;
  List<PokemonList> pokemons;

  PokemonListResponse({this.count, this.next, this.previous, this.pokemons});

  factory PokemonListResponse.fromJson(Map<String, dynamic> json) {
    List<PokemonList> pokemons = json["results"]
        .map<PokemonList>((json) => PokemonList.fromJson(json))
        .toList();

    return PokemonListResponse(
        count: json["count"] as int,
        next: json["next"] as String,
        previous: json["previous"] as String,
        pokemons: pokemons);
  }
}
