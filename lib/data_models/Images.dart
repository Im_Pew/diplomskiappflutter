class Images {
  static const BASE_URL =
      "https://courses.cs.washington.edu/courses/cse154/webservices/pokedex/";

  String photo;
  String typeIcon;
  String weaknessIcon;

  Images({this.photo, this.typeIcon, this.weaknessIcon});

  factory Images.fromJson(Map<String, dynamic> json) {
    return Images(
        photo: BASE_URL + (json["photo"] as String),
        typeIcon: BASE_URL + (json["typeIcon"] as String),
        weaknessIcon: BASE_URL + (json["weaknessIcon"] as String));
  }
}
