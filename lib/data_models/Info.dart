class Info {
  int id;
  String type;
  String weakness;
  String description;

  Info({this.id, this.type, this.weakness, this.description});

  factory Info.fromJson(Map<String, dynamic> json) {
    return Info(
        id: json["id"] as int,
        type: json["type"] as String,
        weakness: json["weakness"] as String,
        description: json["description"] as String);
  }
}
