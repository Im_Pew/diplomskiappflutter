import 'dart:io' show Platform;

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:zavrsniapp/MaterialRouter.dart';
import 'package:zavrsniapp/Router.dart';
import 'package:zavrsniapp/main_list/ListController.dart';

import 'CupertinoRouter.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  Router router = new MaterialRouter();

  @override
  Widget build(BuildContext context) {
    return Platform.isAndroid
        ? MaterialApp(
            title: 'Flutter Demo',
            home: ListController(),
            initialRoute: Router.ROUTE_SPLASH,
            onGenerateRoute: MaterialRouter().generateRoute)
        : CupertinoApp(
            title: "Flutter Demo",
            home: ListController(),
            initialRoute: Router.ROUTE_SPLASH,
            onGenerateRoute: CupertinoRouter().generateRoute,
          );
  }
}
