import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:mvc/mvc.dart';
import 'package:zavrsniapp/main_list/ListModel.dart';
import 'package:zavrsniapp/main_list/ListView.dart';

import '../Router.dart';

class ListController extends MVCController<MainListView, ListModel> {
  ListController() {
    model = ListModel();
    view = MainListView(model);
    init();
  }

  @override
  State<StatefulWidget> createState() => ListControllerState(model);

  @override
  void eventHandler(MVCEvent event) {
    if (event.getValue() != null) {
      if (event.getValue() is String) {
        if (event.getValue() == "refresh") {
          model.fetchPokemons();
        } else {
          model.fetchPokemon(event.getValue() as String);
        }
      }
    }
  }
}

class ListControllerState extends MVCControllerState<ListController>
    with MVCModelObserver {
  ListModel model;

  ListControllerState(ListModel model) {
    this.model = model;
    model.subscribe(this);
  }

  @override
  void initState() {
    super.initState();

    model.fetchPokemons();
  }

  @override
  void update({flag}) {
    if (flag == ListModel.POKEMON) {
      var pokemon = model.getPokemon();

      Navigator.of(context).pushNamed(Router.DETAILS_ROUTE, arguments: pokemon);
    }
  }
}
