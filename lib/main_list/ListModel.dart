import 'dart:convert';
import 'dart:io';

import 'package:mvc/mvc.dart';
import 'package:zavrsniapp/data_models/Pokemon.dart';
import 'package:zavrsniapp/data_models/PokemonList.dart';
import 'package:zavrsniapp/data_models/PokemonListResponse.dart';
import 'package:zavrsniapp/data_provider/RemoteDataProvider.dart';

class ListModel extends MVCModel {
  static const int LOADING_START = 1;
  static const int LOADING_STOP = 2;
  static const int ERROR = 0;
  static const int POKEMON = 100;
  static const int POKEMONS = 101;

  RemoteDataProvider _dataProvider = RemoteDataProvider();

  String _body;
  String _error;

  List<PokemonList> _pokemons;
  Pokemon _pokemon;

  List<PokemonList> getPokemons() => _pokemons;

  Pokemon getPokemon() => _pokemon;

  String getErrorMessage() => _error;

  Future<void> fetchPokemons() async {
    notify(flag: LOADING_START);
    var result;
    try {
      result = await _dataProvider.fetchAllPokemons();
    } on SocketException catch (_) {
      _error = "No internet connection!";
      notify(flag: LOADING_STOP);
      notify(flag: ERROR);

      return;
    }

    if (result.statusCode > 399) {
      _error = result.body;
      notify(flag: LOADING_STOP);
      notify(flag: ERROR);
    } else {
      this._body = result.body;

      this._pokemons = PokemonListResponse.fromJson(jsonDecode(_body)).pokemons;

      notify(flag: LOADING_STOP);
      notify(flag: POKEMONS);
    }
  }

  Future<void> fetchPokemon(String name) async {
    notify(flag: LOADING_START);
    var result;
    try {
      result = await _dataProvider.fetchPokemon(name);
    } on SocketException catch (_) {
      _error = "No internet connection!";
      notify(flag: LOADING_STOP);
      notify(flag: ERROR);

      return;
    }

    if (result.statusCode > 399) {
      _error = result.body;
      notify(flag: LOADING_STOP);
      notify(flag: ERROR);
    } else {
      this._body = result.body;
      this._pokemon = Pokemon.fromJson(jsonDecode(_body));

      notify(flag: LOADING_STOP);
      notify(flag: POKEMON);
    }
  }
}
