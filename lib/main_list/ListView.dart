import 'dart:async';
import 'dart:io' show Platform;

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:mvc/mvc.dart';
import 'package:mvc/mvc_core/MVCState.dart';
import 'package:zavrsniapp/data_models/PokemonList.dart';
import 'package:zavrsniapp/main_list/ListModel.dart';

class MainListView extends MVCStatefulView {
  MainListView(MVCModel model) : super(model);

  List<PokemonList> pokemons = List<PokemonList>();
  List<PokemonList> filteredPokemons = List<PokemonList>();

  @override
  State<StatefulWidget> createState() => ListViewState(model);
}

class ListViewState extends MVCState<MainListView, ListModel> {
  ListViewState(model) : super(model) {
    model.subscribe(this);
  }

  bool isLoading = true;
  String error;
  bool typing = false;
  String search = "";

  @override
  Widget build(BuildContext context) {
    return Platform.isAndroid
        ? Scaffold(
            appBar: AppBar(
              backgroundColor: Color.fromARGB(255, 12, 12, 12),
              title: typing
                  ? searchBox()
                  : Text(search == "" ? "Pokedex" : search,
                      style: titleStyle()),
              leading: IconButton(
                icon: Icon(typing ? Icons.done : Icons.search),
                onPressed: () {
                  setState(() {
                    typing = !typing;
                  });
                },
              ),
            ),
            body: body())
        : CupertinoPageScaffold(
            navigationBar: CupertinoNavigationBar(
                backgroundColor: Color.fromARGB(255, 12, 12, 12),
                leading: CupertinoButton(
                  child: Icon(typing ? Icons.done : Icons.search),
                  onPressed: () {
                    setState(() {
                      typing = !typing;
                    });
                  },
                ),
                middle: typing
                    ? searchBox()
                    : Text(search == "" ? "Pokedex" : search,
                        style: titleStyle())),
            child: Scaffold(
                body: SafeArea(
              child: body(),
            )));
  }

  TextStyle titleStyle() {
    return TextStyle(
        fontFamily: "8-BIT", color: Color.fromARGB(255, 0, 255, 0));
  }

  Widget searchBox() {
    return Container(
      alignment: Alignment.centerLeft,
      color: Color.fromARGB(255, 12, 12, 12),
      child: Platform.isIOS
          ? CupertinoTextField(onChanged: filter, style: titleStyle())
          : TextField(
              onChanged: filter,
              style: titleStyle(),
              decoration: InputDecoration(
                  border: InputBorder.none,
                  hintText: 'Search',
                  hintStyle: TextStyle(color: Colors.white)),
            ),
    );
  }

  void filter(String text) {
    setState(() {
      this.search = text;

      if (text == "") {
        widget.filteredPokemons = widget.pokemons;
      } else {
        widget.filteredPokemons = List<PokemonList>();
        widget.pokemons.forEach((element) {
          if (element.codeName.toLowerCase().contains(text.toLowerCase())) {
            widget.filteredPokemons.add(element);
          }
        });
      }
    });
  }

  Future<String> onRefresh() {
    widget.notifyController(MVCEvent(null, "refresh"));
    return Future.value("sucess");
  }

  Widget body() {
    return Container(
        color: Color.fromARGB(255, 240, 240, 240),
        child: Stack(children: [
          Container(
              color: Color.fromARGB(255, 12, 12, 12),
              child: Platform.isAndroid
                  ? RefreshIndicator(
                      child: materialList(),
                      onRefresh: onRefresh,
                    )
                  : CustomScrollView(
                      slivers: <Widget>[
                        CupertinoSliverRefreshControl(onRefresh: onRefresh),
                        cupertinoList()
                      ],
                    )),
          Visibility(
              visible: isLoading,
              child: Container(
                  color: Color.fromARGB(140, 0, 0, 0),
                  alignment: Alignment.center,
                  child: CircularProgressIndicator())),
          Visibility(
              visible: error != null,
              child: Container(
                  color: Color.fromARGB(140, 0, 0, 0),
                  alignment: Alignment.center,
                  child: Text(
                    error ?? "",
                    style: TextStyle(color: Color.fromARGB(255, 0, 0, 0)),
                  ))),
        ]));
  }

  Widget cupertinoList() {
    return SliverList(
      delegate: SliverChildBuilderDelegate((context, index) {
        return GestureDetector(
            child: listItem(widget.filteredPokemons[index]),
            onTap: () {
              widget.notifyController(
                  MVCEvent(null, widget.filteredPokemons[index].codeName));
            });
      }, childCount: widget.filteredPokemons.length),
    );
  }

  Widget materialList() {
    return ListView.builder(
        itemCount: widget.filteredPokemons.length,
        itemBuilder: (context, index) {
          return GestureDetector(
              child: listItem(widget.filteredPokemons[index]),
              onTap: () {
                widget.notifyController(
                    MVCEvent(null, widget.filteredPokemons[index].codeName));
              });
        });
  }

  Widget listItem(PokemonList pokemon) {
    return Container(
        color: Color.fromARGB(255, 12, 12, 12),
        child: Padding(
            padding: EdgeInsets.all(12),
            child: Row(
              children: <Widget>[
                Image.network(pokemon.url),
                Text(pokemon.name,
                    style: TextStyle(
                        fontFamily: "8-BIT",
                        color: Color.fromARGB(255, 0, 255, 0)))
              ],
            )));
  }

  Widget loadingIndicator() {
    return CircularProgressIndicator();
  }

  @override
  void update({flag}) {
    if (flag == ListModel.LOADING_START) {
      setState(() {
        isLoading = true;
      });
    } else if (flag == ListModel.LOADING_STOP) {
      setState(() {
        isLoading = false;
      });
    } else if (flag == ListModel.POKEMONS) {
      setState(() {
        widget.pokemons = model.getPokemons();
        widget.filteredPokemons = widget.pokemons;
        search = "";
      });
    } else if (flag == ListModel.ERROR) {
      setState(() {
        this.error = model.getErrorMessage();
        Scaffold.of(context)
            .showSnackBar(SnackBar(content: Text(model.getErrorMessage())));
      });
      Future.delayed(Duration(milliseconds: 1500), () {
        setState(() {
          this.error = null;
        });
      });
    }
  }
}
