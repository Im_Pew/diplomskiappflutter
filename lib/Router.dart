import 'package:flutter/widgets.dart';

abstract class Router {
  static const String INITIAL_ROUTE = "initial_route";
  static const String ROUTE_SPLASH = "splash";
  static const String DETAILS_ROUTE = "details_route";

  Route<dynamic> generateRoute(RouteSettings settings);
}