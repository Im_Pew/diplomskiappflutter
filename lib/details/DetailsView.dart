import 'dart:io' show Platform;

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:mvc/mvc.dart';
import 'package:zavrsniapp/data_models/Pokemon.dart';

class DetailsView extends MVCStatelessView {
  Pokemon _pokemon = Pokemon();

  DetailsView(MVCModel model, {Pokemon pokemon}) : super(model) {
    this._pokemon = pokemon;
  }

  @override
  Widget build(BuildContext context) {
    return Platform.isAndroid
        ? Scaffold(
            appBar: AppBar(
              backgroundColor: Color.fromARGB(255, 204, 0, 0),
              title: Text(_pokemon.name),
              leading: BackButton(onPressed: () {
                notifyController(MVCEvent(null, null));
              }),
            ),
            body: body())
        : CupertinoPageScaffold(
            navigationBar: CupertinoNavigationBar(
              backgroundColor: Color.fromARGB(255, 204, 0, 0),
              middle: Text(_pokemon.name),
            ),
            child: SafeArea(child: body()),
          );
  }

  Widget body() => Container(
      color: Color.fromARGB(255, 204, 0, 0),
      child: Column(
        children: <Widget>[
          Container(
            constraints: BoxConstraints.expand(height: 200, width: 320),
            color: Color.fromARGB(255, 128, 127, 128),
            child: Padding(
                padding: EdgeInsets.all(4),
                child: Container(
                    color: Color.fromARGB(255, 255, 255, 255),
                    child: Image.network(_pokemon.images.photo))),
          ),
          Container(
              constraints: BoxConstraints.expand(height: 32, width: 320),
              color: Color.fromARGB(255, 128, 127, 128),
              alignment: Alignment.center,
              child: Padding(
                  padding: EdgeInsets.all(4),
                  child: Container(
                    color: Color.fromARGB(255, 255, 255, 255),
                    child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: <Widget>[
                          Spacer(),
                          Text("Type"),
                          Container(
                              constraints:
                                  BoxConstraints(maxWidth: 24, maxHeight: 24),
                              child: Image.network(_pokemon.images.typeIcon)),
                          Spacer(),
                          Text("Weakness"),
                          Container(
                              constraints:
                                  BoxConstraints.expand(width: 24, height: 24),
                              child:
                                  Image.network(_pokemon.images.weaknessIcon)),
                          Spacer()
                        ]),
                  ))),
          Container(
              constraints: BoxConstraints.expand(height: 32, width: 320),
              color: Color.fromARGB(255, 128, 127, 128),
              child: Padding(
                  padding: EdgeInsets.all(12), child: Text(_pokemon.name))),
          Container(
              constraints: BoxConstraints.expand(height: 32, width: 320),
              color: Color.fromARGB(255, 128, 127, 128),
              child: Padding(
                  padding: EdgeInsets.all(12),
                  child: Text(_pokemon.hp.toString()))),
          Container(
              constraints: BoxConstraints.expand(height: 32, width: 320),
              color: Color.fromARGB(255, 128, 127, 128),
              child: Padding(
                  padding: EdgeInsets.all(12),
                  child: Text(_pokemon.info.description))),
        ],
      ),
      alignment: Alignment.center);
}
