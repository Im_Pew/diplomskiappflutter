import 'package:flutter/widgets.dart';
import 'package:mvc/mvc.dart';
import 'package:zavrsniapp/data_models/Pokemon.dart';

import 'DetailsModel.dart';
import 'DetailsView.dart';

class DetailsController extends MVCController<DetailsView, DetailsModel> {
  DetailsController({Pokemon pokemon}) {
    model = DetailsModel();
    view = DetailsView(model, pokemon: pokemon);

    init();
  }

  @override
  State<StatefulWidget> createState() => DetailsControllerState();

  @override
  void eventHandler(MVCEvent event) {
    Navigator.pop(context);
  }
}

class DetailsControllerState extends MVCControllerState {}
