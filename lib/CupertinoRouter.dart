import 'package:flutter/cupertino.dart';
import 'package:zavrsniapp/Router.dart';

import 'details/DetailsController.dart';
import 'main_list/ListController.dart';

class CupertinoRouter extends Router {
  @override
  Route generateRoute(RouteSettings settings) {
    if (settings.name == Router.DETAILS_ROUTE) {
      return CupertinoPageRoute(
          builder: (_) => DetailsController(pokemon: settings.arguments));
    }
    return CupertinoPageRoute(builder: (_) => ListController());
  }
}
